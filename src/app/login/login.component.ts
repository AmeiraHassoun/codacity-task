import { Component , Inject , OnInit} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BrowserModule }    from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
   User = {}
   id = ''

   constructor(
   	private http:HttpClient
   	) { }

     ngOnInit(): void { }

  onClick(){
  	  this.http.post('/login',this.User).subscribe(res => {
        this.id = res.userId;
        window.location.href = 'cashier'
  	  }, (err) =>{
  	  	console.log("qays" , err)
  	  }
  	  );
  }

  test(ss){
  	console.log("s" , ss)
  }
}
