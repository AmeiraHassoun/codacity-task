import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';


import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
import { OwnerComponent } from './owner/owner.component';
import { CashierComponent } from './cashier/cashier.component';

const appRoutes: Routes = [
  { path: '',
    component: MainComponent
  },
  { path: 'login',
    component: LoginComponent
  },
  { path: 'owner',
    component: OwnerComponent
  },
  { path: 'cashier',
    component: CashierComponent
  },

]


@NgModule({
  declarations: [
   AppComponent,
    MainComponent,
    LoginComponent,
    OwnerComponent,
    CashierComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

const routes: Routes = [
  { path: '', component: MainComponent }
];