const express = require('express')
const db = require('./database/index.js')
const helpers = require('./database/helper.js')
const app = express()

var morgan = require('morgan');
var parser = require('body-parser');

app.use(express.static(__dirname + "/dist/pointOfSale"))
app.use(morgan('dev'));
app.use(parser.json());
module.exports.app = app;

app.set('port',3000)

app.post('/login', function (req,res) {
	var email = req.body.email
	helpers.getId(email, function(err,result){
		if(err){
			throw err		
		}else{
				res.json(result)
			}	
		})
})


app.post('/history', function(req,res){
	var userId = req.body.userId;
	var cost = req.body.cost
	var payment = req.body.payment

	var changes = helpers.Change(cost,payment);
	var args = [userId,cost,payment,changes]
	console.log('post' , changes)

	helpers.postHistory(args, function(error, result){
		if(error){
			throw error
		}else{
			helpers.getHistory(userId, function(err, results){
				if(err){
					throw error;
				}else{
					res.send(results)
				}
			})
		}
	})
})


app.post('/getHistoryByName',function(req,res){
	var name = req.body.name;

	helpers.getIdByName(name, function(error,results){
		if(error){throw error}
		var id = results[0].userId

		helpers.getHistoryById(id,function(error,result){
			if(error){
				throw error
			}else{
				res.json(result)
			}
		})
	})

})

app.post('/transactions', function(req,res){
	var bills = [50,20,10,5,1]
	var historyId = req.body.historyId
	var changes = req.body.changes
	var payment = req.body.payment

	var deposite = helpers.calTrans(payment) //object
	var withdraw = helpers.calTrans(changes) //object
	
	for(var i=0; i<bills.length; i++){
		if(deposite[bills[i]] !== undefined){
			var amount = deposite[bills[i]]+'*'+bills[i]
			var args = [historyId,amount,'deposite']


			helpers.postTrans(args, function(err,results){
				if(err){
					throw err
				}else{
					console.log("saved")
				}
			})
		}
	}

	for(var i=0; i<bills.length; i++){
		if(withdraw[bills[i]] !== undefined){
			var amount = withdraw[bills[i]]+'*'+bills[i]
			var args = [historyId,amount,'withdraw']
			helpers.postTrans(args, function(err,results){
				if(err){
					throw err
				}else{
					console.log("saved")
				}
			})
		}
	}
})






app.get('/', function(req,res){
	res.sendFile("index.html")
})





app.listen(3000, () => console.log('Example app listening on port 3000!'))
