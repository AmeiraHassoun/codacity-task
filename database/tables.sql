CREATE DATABASE logs;
USE logs;


CREATE TABLE cashiers (
  userId int NOT NULL AUTO_INCREMENT,
  firstName varchar(10)  NOT NULL,
  lastName varchar(10)  NOT NULL,
  fullName varchar(50) NOT NULL,
  email varchar(50) NOT NULL,
  PRIMARY KEY (userId)
);

CREATE TABLE history (
	historyId int NOT NULL AUTO_INCREMENT,
	userId INTEGER,
	cost INTEGER NOT NULL,
	payment INTEGER NOT NULL,
	changes INTEGER NOT NULL,
	datum DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY (historyId),
	FOREIGN KEY (userId) REFERENCES cashiers(userId)
);

CREATE TABLE transactions (
	historyId int,
	amount varchar(20) NOT NULL,
	types varchar(10) NOT NULL,
	FOREIGN KEY (historyId) REFERENCES history(historyId)
);

INSERT INTO cashiers (firstName, lastName, fullName, email) VALUES 
("Ameira","Hassoun","AmeiraHassoun","ameira@gmail.com"),
("Qays","Trad","QaysTrad","qays@gmail.com"),
("Alaa","Migdady","AlaaMigdady","alaa@gmail.com"),
("Tal","Omari","TalOmari","tal@gmail.com");

