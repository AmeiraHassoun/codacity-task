var db = require ('./index.js')
var mysql = require('mysql')

function getId (arg, callback){
	var command = mysql.format('SELECT userId from cashiers WHERE email = ?', [arg])
	db.query(command, function(err,results){
		callback(err, results)
	})
}

function Change(cost, payment){
	var changes =  payment - cost;
	return changes;
}

function postHistory (params, callback){
	var queryStr = mysql.format('INSERT INTO history (userId,cost,payment,changes) VALUES (?,?,?,?)');
	db.query(queryStr,params,function(error,results){
		callback(error,results)
	})
	console.log('postHistory')
}

function getHistory(param, callback){
	var queryStr = mysql.format('SELECT historyId,changes FROM history WHERE userId = ?',[param])
	db.query(queryStr, function(err, results){
		callback(err,results)
	})
}


function calTrans (amount){
	var arr = [50,20,10,5,1]
	var obj = {}
	while(amount>0){
		for(var i=0; i<arr.length;i++){
			console.log(amount)
			if((amount - arr[i])>=0){
				if(obj[arr[i]] === undefined){
					obj[arr[i]] = 1
				}else{
					obj[arr[i]]++
				}
				amount = amount - arr[i]
			}	
		}
	}
	return obj
}


function postTrans(args,callback){
	var queryStr = mysql.format('INSERT INTO transactions (historyId,amount,types) VALUES (?,?,?)') 
	db.query(queryStr,args,function(error,results){
		callback(error,results)
	})
}


function getHistoryById(arg,callback){
	var queryStr = mysql.format('SELECT history.cost,history.payment,history.changes,history.datum FROM history WHERE history.userId=?',[arg])
	db.query(queryStr,function(error,results){
		callback(error,results)
	})
}

function getIdByName(name,callback){
	var queryStr = mysql.format('SELECT userId from cashiers WHERE fullName=?',[name])
	db.query(queryStr,function(error,results){
		callback(error,results)
	})
}

module.exports.getId = getId
module.exports.Change = Change
module.exports.postHistory = postHistory
module.exports.getHistory = getHistory
module.exports.calTrans = calTrans
module.exports.postTrans = postTrans
module.exports.getHistoryById = getHistoryById
module.exports.getIdByName = getIdByName

